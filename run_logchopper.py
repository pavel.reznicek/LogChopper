#!/usr/bin/env python3

import argparse

import logchopper

parser = argparse.ArgumentParser(description='The Log Chopper, '
  'system log parser and analyzer.')

parser.add_argument('--syntax', help='log file syntax', 
  default=logchopper.SYNTAX_PHP_MAIL)
parser.add_argument('file', nargs='?', help='input file')

args = parser.parse_args()

#print("syntax:", args.syntax)
#print("input file:", args.file)

#app = logchopper.ChopperApp(syntax=args.syntax, log_file_path=args.file)
logchopper.ChopperApp(syntax=args.syntax, log_file_path=args.file)


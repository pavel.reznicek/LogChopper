from setuptools import setup, find_packages

setup (
       name='LogChopper',
       version='0.1',
       packages=find_packages(),

       # Declare your packages' dependencies here, for eg:
       install_requires=['wx'],

       # Fill in these to make your Egg ready for upload to
       # PyPI
       author='Pavel Řezníček',
       author_email='pavel.reznicek@evangnet.cz',

       #summary = 'Just another Python package for the cheese shop',
       url='',
       license='GNU/GPL v. 3',
       long_description='A system log analysis tool based on regular '\
            'expressions and an SQLite database',

       # could also include long_description, download_url, classifiers, etc.

  
       )
# LogChopper
A system log parser and analyser based on syntax definitions and SQL queries

Short help
==========

First, copy the database file logchopper.empty.sqlite to logchopper.sqlite.
I don’t want to leak a huge amount of privacy sensitive data to the public
so I don’t publish the actual working database.

Launch the application by running the run_logchopper.py script.
The main application window should show up.

In the upper part of the window, select the syntax of your choice.

The syntaxes are defined in files ending with the .syntax extension in the application folder.

Each syntax file should describe every single line in the log file that's
going to be analysed.

Select the log file and click on the "Import" button. The log lines should
then get imported into the database (stored in the "logchopper.sqlite" file).

Once the import is finished, a basic SQL select statement is executed upon the
imported data in order to present it to you.

The table where all the data gets stored has to have the same name as the chosen log file syntax.

You can play around with the data using SQL queries. To run the query, press
the "Execute SQL" button.

Before you can use a new syntax, you have to create a table named after it
with fields that correspond to all the named regex groups you define in the
syntax file. There has to be just one more field, called line, which should be
a primary key integer. It simply holds the log line number, serving as a key.

Tools & libraries used
======================

I chose SQLite3 as the database background. This should be basically a lightweight analysis tool that you can take with you on a flash drive.
So you should be able to take the database and go.

Then, I chose wxPython as the GUI frontend library. I use it for years.
So you have to install it before you use this program.

And, last but not least, the most important is the Python language
interpreter. I compiled wxPython Phoenix for Python 3 myself but the code
is so universal that it can run on Python 2.7 too so you can use the standard pre-built binaries for wxPython.

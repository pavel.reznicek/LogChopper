#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import re
import sqlite3
import wx
import wx.stc
import wx.grid

SYNTAX_PHP_MAIL = 'php_mail'

def ourpath(filename):
  # Get module path
  modulepath = __file__
  # Get module directory
  moduledir = os.path.dirname(modulepath)
  #print("module directory:", moduledir)
  result = os.path.join(moduledir, filename)
  return result

def get_syntax_list():
  moduledir = ourpath('')
  filelist = os.listdir(moduledir)
  ext = ".syntax"
  result = [os.path.splitext(f)[0] for f in filelist if f.endswith(ext)]
  result.sort()
  return result

def show_error(e, message, gui=True):
  if not e:
    complete_message = message
  else:
    complete_message = """%(message)s
Error class: %(classname)s
Message: %(e)s"""%{
      'message': message,
      'classname': e.__class__.__name__,
      'e': e
      }
  if gui:
    wx.MessageBox(message=complete_message, caption="Error",
      style=wx.OK | wx.CENTRE | wx.ICON_ERROR)
  else:
    print(complete_message)

def chop(syntax, log_file_path, gui=True):
  # Get syntax file name
  syntax_file_name = syntax + ".syntax"
  syntax_file_path = ourpath(syntax_file_name)
  #print("syntax file path:", syntax_file_path)

  if os.path.exists(syntax_file_path):
    #print("The syntax file exists!")
    try:
      syntax_file = open(syntax_file_path, 'r')
    except Exception as e:
      show_error(e, "Couldn't open the syntax file!", gui)
      return
    # Read the regex pattern from the syntax file
    pattern = syntax_file.read()
    # Create a regex object by compiling the pattern
    try:
      rex = re.compile(pattern, flags=re.VERBOSE)
    except Exception as e:
      show_error(e, "Couldn't compile the syntax file “%s”."%syntax_file_path,
        gui)
      return
    #print(re)
    if os.path.exists(log_file_path):
      try:
        log_file = open(log_file_path, 'r', errors="replace")
      except Exception as e:
        show_error(e, "Error while opening file %s:"%log_file_path, gui)
        return
      # Open the database
      db = ChopperDB(syntax)
      # Clear the table of the corresponding syntax
      db.clear()
      # Save the log lines to the database
      line_number = 1
      # @type log_file file
      for line in log_file.readlines():
        match = rex.search(line)
        if match:
          line_dict = match.groupdict()
          db.import_line_dict(line_number, line_dict)
        print(line_number)
        line_number += 1
      db.commit()
    else:
      show_error(None, "The input file “%s” doesn’t exist."%log_file_path, gui)
      return
  else:
    show_error(None, "The syntax file of the given log file syntax doesn’t "
      "exist!")
    return

def get_fields(cursor):
  description = cursor.description
  if description:
    result = [d[0] for d in description if d and len(d) > 0]
    return result

class ChopperDB(sqlite3.Connection):
  def __init__(self, syntax, gui=True):
    db_file_path = ourpath('logchopper.sqlite')
    sqlite3.Connection.__init__(self, db_file_path)
    self.syntax = syntax
    #self.clear()

  def clear(self):
    self.execute('delete from "%s"'%self.syntax)
    self.commit()

  def import_line_dict(self, line_number, line_dict):
    #print(line_number, line_dict)
    fields = '"line"'
    qmarks = '?'
    values = (line_number,)
    for f in line_dict.keys():
      if fields:
        fields += ", "
      fields += '"%s"'%f
      if qmarks:
        qmarks += ", "
      qmarks += '?'
      values += (line_dict[f],)
    sql = 'insert into "%s" (%s) values (%s)'%(self.syntax, fields, qmarks)
    #print sql
    self.execute(sql, values)

class ChopperForm(wx.Frame):
  def __init__(self, parent=None, syntax=SYNTAX_PHP_MAIL,
      log_file_path=None):
    wx.Frame.__init__(self, parent=parent, title="Log Chopper", size=(600, 400))

    self.MinSize = self.Size

    # Assign "properties"

    self.syntax = syntax
    self.log_file_path = log_file_path

    # Main sizer

    main_sizer = self.main_sizer = wx.BoxSizer(wx.VERTICAL)
    self.Sizer = main_sizer

    # Tools panel

    tools_panel = self.tools_panel = wx.Panel(parent=self,
      style=wx.BORDER_RAISED)
    main_sizer.Add(tools_panel, 0, wx.GROW)

    tools_panel_sizer = self.tools_panel_sizer = wx.BoxSizer(wx.VERTICAL)
    tools_panel.Sizer = tools_panel_sizer

    # File selection panel

    file_selection_panel = self.file_selection_panel = wx.Panel(
      parent=tools_panel, style = wx.BORDER_RAISED)
    tools_panel_sizer.Add(file_selection_panel, 0, wx.GROW)

    file_selection_panel_sizer = self.file_selection_panel_sizer = wx.BoxSizer(
      wx.HORIZONTAL)
    file_selection_panel.Sizer = file_selection_panel_sizer

    #Syntax combo

    syntax_label = self.syntax_label = wx.StaticText(file_selection_panel,
      label="&Syntax:")
    file_selection_panel_sizer.Add(syntax_label, 0, wx.CENTER | wx.ALL, 8)

    syntax_combo_items = get_syntax_list()
    syntax_combo = self.syntax_combo = wx.ComboBox(parent=file_selection_panel,
      value=syntax, choices=syntax_combo_items, style=wx.CB_READONLY)
    file_selection_panel_sizer.Add(syntax_combo, 1, wx.GROW | wx.TOP |
      wx.BOTTOM | wx.RIGHT, 8)

    # File picker

    file_picker_label = self.file_picker_label = wx.StaticText(
      file_selection_panel, label="Log &File:")
    file_selection_panel_sizer.Add(file_picker_label, 0, wx.CENTER | wx.TOP |
      wx.BOTTOM | wx.RIGHT, 8)

    file_picker = self.file_picker = wx.FilePickerCtrl(
      parent=file_selection_panel, message="Select a log file",
      wildcard='Log Files (*.log)|*.log|All Files (*)|*')
    if log_file_path:
      if os.path.exists(log_file_path):
        file_picker.Path = log_file_path
    file_selection_panel_sizer.Add(file_picker, 1, wx.GROW | wx.TOP |
      wx.BOTTOM | wx.RIGHT, 8)

    # Import button

    import_button = self.import_button = wx.Button(file_selection_panel,
      label="&Import")
    import_button.Enabled = self.can_import()
    file_selection_panel_sizer.Add(import_button, 0, wx.GROW | wx.TOP |
      wx.BOTTOM | wx.RIGHT, 8)

    # SQL panel

    sql_panel = self.sql_panel = wx.Panel(parent=tools_panel,
      style=wx.BORDER_RAISED)
    tools_panel_sizer.Add(sql_panel, 0, wx.GROW)

    sql_panel_sizer = self.sql_panel_sizer = wx.BoxSizer(wx.HORIZONTAL)
    sql_panel.Sizer = sql_panel_sizer

    # Styled text control for an SQL query

    sql_stc = self.sql_stc = wx.stc.StyledTextCtrl(sql_panel,
      #style=wx.stc.USE_TEXTCTRL
      )
    sql_stc.Lexer = wx.stc.STC_LEX_SQL
    sql_stc.StyleSetFaceName(wx.stc.STC_STYLE_DEFAULT, "Monospace")
    sql_stc.StyleSetForeground(wx.stc.STC_SQL_IDENTIFIER, wx.BLUE)
    sql_stc.StyleSetForeground(wx.stc.STC_SQL_CHARACTER, wx.Colour(255, 128,
      128))
    sql_stc.StyleSetForeground(wx.stc.STC_SQL_NUMBER, wx.Colour(0, 128, 128))
    sql_stc.StyleSetBackground(wx.stc.STC_SQL_COMMENT, wx.Colour(192, 192, 192))
    sql_stc.StyleSetForeground(wx.stc.STC_SQL_COMMENT, wx.Colour(64, 64, 64))
    sql_stc.StyleSetBackground(wx.stc.STC_SQL_COMMENTLINE, wx.Colour(192, 192,
      192))
    sql_stc.StyleSetForeground(wx.stc.STC_SQL_COMMENTLINE, wx.Colour(64, 64,
      64))
    sql_stc.StyleSetForeground(wx.stc.STC_SQL_OPERATOR, wx.Colour(255, 0, 255))
    sql_panel_sizer.Add(sql_stc, 1, wx.GROW)

    # Button to run the SQL query

    sql_exec_button = self.sql_exec_button = wx.Button(sql_panel,
      label="&Execute SQL")
    sql_exec_button.Enabled = False
    sql_panel_sizer.Add(sql_exec_button, 0, wx.CENTER | wx.ALL, 8)

    # Grid

    grid = self.grid = wx.grid.Grid(self)
    main_sizer.Add(grid, 1, wx.GROW)

    # Export panel
    export_panel = self.export_panel = wx.Panel(parent=self, 
      style=wx.BORDER_RAISED)
    main_sizer.Add(export_panel, 0, wx.GROW)

    export_panel_sizer = self.export_panel_sizer = wx.BoxSizer(wx.HORIZONTAL)
    export_panel.Sizer = export_panel_sizer
    
    # Button to export the data in the grid
    csv_export_button = self.csv_export_button = wx.Button(export_panel,
      label="Export to CSV")
    csv_export_button.Enabled = False
    export_panel_sizer.Add((0,0), 1, wx.GROW)
    export_panel_sizer.Add(csv_export_button, 0, wx.GROW | wx.ALL, 8)

    # Bind event handelrs

    self.Bind(wx.EVT_SHOW, self.on_show)
    syntax_combo.Bind(wx.EVT_COMBOBOX, self.on_syntax_combo)
    file_picker.Bind(wx.EVT_FILEPICKER_CHANGED, self.on_file_picker)
    import_button.Bind(wx.EVT_BUTTON, self.on_import_button)
    sql_exec_button.Bind(wx.EVT_BUTTON, self.on_sql_exec_button)
    csv_export_button.Bind(wx.EVT_BUTTON, self.on_csv_export_button)

  def can_import(self):
    return bool(self.syntax and self.log_file_path)

  def exec_sql(self):
    sql = self.sql_stc.Text
    #wx.MessageBox(sql)
    db = ChopperDB(self.syntax)
    try:
      cursor = db.execute(sql)
    except Exception as e:
      show_error(e, "An error occured while executing the SQL query:")
      self.csv_export_button.Enabled = False
      return
    query_result = cursor.fetchall()
    if query_result:
      fields = get_fields(cursor)
      if not self.grid.Table:
        self.grid.CreateGrid(numRows=0, numCols=0)
      # Remove all rows
      if self.grid.NumberRows > 0:
        self.grid.DeleteRows(numRows=self.grid.NumberRows)
      # Remove all colums
      if self.grid.NumberCols > 0:
        self.grid.DeleteCols(numCols=self.grid.NumberCols)
      # Add columns
      self.grid.AppendCols(numCols=len(fields))
      # Add rows
      self.grid.AppendRows(numRows=len(query_result))
      # Set field header captions
      for f in range(len(fields)):
        self.grid.SetColLabelValue(f, fields[f])
      # Fill cells
      for r in range(len(query_result)):
        row = query_result[r]
        for c in range(len(row)):
          cell_value = row[c]
          self.grid.SetCellValue(r, c, str(cell_value))
      # Arrange the column and row sizes to fit their contents
      self.grid.AutoSizeColumns(setAsMin=False)
      # Enable the CSV export button
      self.csv_export_button.Enabled = True
    else:
      self.csv_export_button.Enabled = False
    cursor.close()

  def export_to_csv(self):
    f = open('logchopper.csv', 'w+')
    for y in range(self.grid.NumberRows):
        line = ""
        for x in range(self.grid.NumberCols):
          if line:
            line += ","
          line += self.grid.GetCellValue(y, x)
        f.write(line + os.linesep)
    f.close()
    from cigydd import Pyp
    Pyp()


  def on_show(self, evt):
    #wx.MessageBox(
    #  "syntax: “%s”%slog file path: “%s”"%(
    #    self.syntax, os.linesep, self.log_file_path))
    pass

  def on_syntax_combo(self, evt):
    self.syntax = self.syntax_combo.Value
    self.import_button.Enabled = self.can_import()

  def on_file_picker(self, evt):
    self.log_file_path = self.file_picker.Path
    self.import_button.Enabled = self.can_import()

  def on_import_button(self, evt):
    chop(self.syntax, self.log_file_path)
    from cigydd import Pyp
    Pyp()
    self.sql_exec_button.Enabled = True
    self.sql_stc.Text = 'select * from "%s"'%self.syntax
    self.exec_sql()

  def on_sql_exec_button(self, evt):
    self.exec_sql()

  def on_csv_export_button(self, evt):
    self.export_to_csv()


class ChopperApp(wx.App):
  def __init__(self, syntax=SYNTAX_PHP_MAIL, log_file_path=None):
    wx.App.__init__(self, redirect=False)
    form = ChopperForm(syntax=syntax, log_file_path=log_file_path)
    form.Show()
    self.TopWindow = form
    self.MainLoop()
